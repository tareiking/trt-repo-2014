<article <?php hybrid_attr( 'post' ); ?>>

	<div class="entry-wrap">

		<div class="wrap">

			<div <?php hybrid_attr( 'entry-content' ); ?>>
				<?php the_content(); ?>
				<?php edit_post_link(); ?>
			</div><!-- .entry-content -->

		</div><!-- .wrap -->

	</div><!-- .entry-wrap -->

</article><!-- .entry -->
