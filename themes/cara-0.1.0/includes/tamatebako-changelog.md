## Tamatebako Changelog:

### 1.1.0
* for HC 2.0.1
* remove dots from content more
* add more inline docs.
* HTML5 support now added via HC
* reset css: gallery icon img add hover opacity.

### 1.0.0
* first stable release