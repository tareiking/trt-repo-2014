# Cara WordPress Theme

## About:

**Author:** David Chandra Purnama ( [@turtlepod](http://twitter.com/turtlepod) / [shellcreeper.com](http://shellcreeper.com/) )

Cara theme is an clean and simple grid responsive theme built with search-engine optimization (SEO) in mind by utilizing the most current HTML5 conventions and [Schema.org](http://schema.org) microdata.

## Copyright & license

This theme is licensed under the [GNU General Public License](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html), version 2 (GPL) or later.

2014 © [Genbu Media](http://genbu.me/). All rights reserved.

## Changelog:

### 0.1.0
* Initial version
