Our Marae Connect Theme
=======================

This theme is built for Our Marae WordPress multisite and is designed to offer some basic customisation options to site owners.

# Roadmap V2.0

- [ ] Add a slider support for http://github.com/sennza/pico-slider
- [ ] Add Pagination styles
- [ ] Add WooCommerce template support
- [ ] Add support for OM Events: http://github.com/tareiking/om-events/

# Roadmap V1 Features - Complete, yay!

## mobile

- [x] top bar and menu location
- [x] 'back' font too big
- [x] content area needs padding to display background image
- [x] reduce margin on content area to accomodate above

## menu

- [x] shadow
- [x] content shadow

## backstretch

- [x] add backstretch.js to background image where non IE

## widgets

- [x] footer

## customiser

- [x] add tab colors
- [x] add primary colors
- [x] logo size
- [x] footer font colors
- [x] remove background from flashing before backstretch does its thing

## featured image

- [x] add image sizes for featured image
- [x] add description in meta box


## Search Form
- [x] style searchbar

## things

- image captions
- [x] buttons and entry meta
- [x] recent comments
- [ ] pagination

## templates
- [x] full width
- [x] three column
- [2.0] shop

## blog
- [x] list
- [x] single

# tinymce
- [x] Add buttons for fonts
- [x] Add styles to content style

## Support for OM Events

- [2.0] template support: events list

- [2.0] portfolio: http://organicthemes.com/demo/connect/portfolio/#blank
