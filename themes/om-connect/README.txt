OM Connect WordPress Theme
====================================================

OM Connect is a 'lite' version of Organic Themes Connect Theme and has been built for usage on the Ourmarae.com network.

This theme has been made freely available with pending permissions and accreditations from Organic Themes.

This particular theme is built with Zurb's Foundation 5 Framework and Sennza Flair starter theme. This theme can be directly
forked from Github at http://github.com/tareiking/om-connect-theme. As always, patches are welcome.