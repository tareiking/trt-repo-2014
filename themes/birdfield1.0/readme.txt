== Description ==
BirdFIELD is a responsive web design theme. Feature fullscreen and parallax custom image, and fixed header. The homepage displays with tagged news and the grid posts. You can choose the text color, link color, header background color by theme options.


=== Tags ===
one-columns, flexible-width, custom-background, custom-colors, custom-header, custom-menu, editor-style, featured-image-header, featured-images, full-width-template, sticky-post, theme-options, threaded-comments, translation-ready


=== Features ===
Responsive Layout, Theme Customize, Page Navigation


=== Widgets Areas ===
The Theme has customizable header and footer.


= Usage =
You can upload the header image by custom-header, and set the Widget Area for header.
If the custom menu is short, the header becomes the upper fixed, header image becomes the parallax effect.
When the posts are tagged with "news", display NEWS area on homepage.


== License ==
BirdFIELD WordPress theme, Copyright (C) 2014 Sysbird
The theme is licensed under the GPL.
License: GNU General Public License v2.0
License URI: http://www.gnu.org/licenses/gpl-2.0.html
The script html5.js and jquery.tile.min.js are released under the  MIT License.
The Font Awesome is released under the GPL license.
The headers are created by theme author and are released under GPL license.


== Author ==
The theme built by TORIYAMA Yuko at Sysbird.
You can contact me at inquiry form.
http://www.sysbird.jp/wptips/contact/


== Changelog ==
v1.0
* Hello, world!
